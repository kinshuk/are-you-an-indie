$(window).load(function () {
    var textArray = ["No.", "Oh Really?", "You think so?", "If You Say So", "You Wish", "Do you want to be?", "Who said so!<br />Was it Jake?", "You Monster", "If you are one,<br />then you're a terrible example of it", "Cool story, bro.", "Nope", "Sorry", "One can only aspire", "You either die an indie<br />or live long enough to become a suit", "You are a disappointment", "Are you seeking validation?", "This is peer-pressure", "Would you kindly focus on the game?"],
        currentHash = window.location.hash.split('#')[1];
    currentText = currentHash !== undefined ? textArray[textArray.indexOf(currentHash.replace(/\-/g, ' '))] : undefined;
    $('#iFrm').randomText(textArray, 1000000, '', undefined, currentText);
});

$.fn.randomText = function (textArray, interval, randomEle, prevText, currentText) {
    var obj = $(this);
    if ($('#iContent').length == 0) {
        obj.append('<div id="iContent">');
    }
    var textCont = $('#iContent');
    if (typeof randomEle != 'undefined') {
        if ($('#iRefresh').length == 0) {
            obj.append('<a href="javascript:;" id="iRefresh"><em>' + randomEle);
        }
    }
    textCont.fadeOut(700, function () {
        var chosenText = (currentText !== undefined) ? currentText : random_array(textArray);
        while (chosenText == prevText) {
            chosenText = random_array(textArray);
        }
        textCont.empty().html(chosenText);
        textCont.fadeIn(500);
        sendText = chosenText;
        window.location.hash = sendText.replace(/\s/g, '-');
    });
    timeOut = setTimeout(function () {
        obj.randomText(textArray, interval, randomEle, sendText);
    }, interval);
    $("#iRefresh").click(function () {
        if (!textCont.is(':animated')) {
            clearTimeout(timeOut);
            obj.randomText(textArray, interval, randomEle, sendText);
        }
    });
    
    $("#iLikeClick").click(function (e) {
        var thisrecord = $(this).closest("#iLike");
        var likes = parseInt(thisrecord.find("#count").html()) + 1;
        thisrecord.find("#count").html(likes);
        $(this).replaceWith("Liked!");
        var id = thisrecord.attr("#count").substring(4);
        e.preventDefault();
    });
}

function random_array(aArray) {
    var rand = Math.floor(Math.random() * aArray.length + aArray.length);
    var randArray = aArray[rand - aArray.length];
    return randArray;
}